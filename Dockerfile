FROM centos:latest

RUN yum update -y \
        && \
    yum install -y \
    curl \
    gnupg \
    epel-release \
    gcc-c++ \
    make

RUN curl -sL https://rpm.nodesource.com/setup_13.x | bash -

RUN yum install -y nodejs \
    npm

EXPOSE 3000

#ADD node-express-hello-world/ /app
ADD . /app

RUN cd /app; npm install; npm run build

CMD ["node", "/app/build/index.js"]


